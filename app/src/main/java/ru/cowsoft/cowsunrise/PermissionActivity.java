package ru.cowsoft.cowsunrise;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import static ru.cowsoft.cowsunrise.CowSunriseWidget.PREF_DAYLENGTH_MIN;
import static ru.cowsoft.cowsunrise.CowSunriseWidget.PREF_LAT;
import static ru.cowsoft.cowsunrise.CowSunriseWidget.PREF_LONG;

public class PermissionActivity extends AppCompatActivity
{
    public static final String PREF_AUTOSTART = "autostart";
    public static final String PREF_BOLDFONT = "bold_font";
    public static final String mypreference = "cowsunrise";
    private SharedPreferences sharedpreferences;

    void updateLocationText()
    {
        final TextView text_lat = findViewById( R.id.textView_lat );
        final TextView text_lon = findViewById( R.id.textView_lon );
        final TextView text_dayLength = findViewById( R.id.text_dayLength );

        text_lat.setText( Float.toString( sharedpreferences.getFloat( PREF_LAT, 0 ) ) );
        text_lon.setText( Float.toString( sharedpreferences.getFloat( PREF_LONG, 0 ) ) );
        text_dayLength.setText( "Day length: " + Integer.toString( sharedpreferences.getInt( PREF_DAYLENGTH_MIN, 0 ) ) + " minutes" );
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        updateLocationText();
    }

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        sharedpreferences = getSharedPreferences( mypreference, Context.MODE_PRIVATE );

        // Такой обход ограничений на запуск в фоне - запускаем активити и закрываем
        if( getIntent().getExtras() != null && getIntent().getExtras().getBoolean( "startService" ) )
        {
            Toast.makeText( this, "CowSunrise started", Toast.LENGTH_LONG ).show();
            moveTaskToBack( true );
            return;
        }

        setContentView( R.layout.permission_activity );

        if( Build.VERSION.SDK_INT == Build.VERSION_CODES.Q )
        {
            String[] permissions_array_q = new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};

            int perm = 0;
            ActivityCompat.requestPermissions( this, permissions_array_q, perm );
        }
        else
        {
            String[] permissions_array_q = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};

            int perm = 0;
            ActivityCompat.requestPermissions( this, permissions_array_q, perm );
        }

        updateLocationText();

        final CheckBox checkbox_autostart = findViewById( R.id.checkBox_autostart );
        final CheckBox checkbox_boldfont = findViewById( R.id.checkBox_bold );
        final TextView text = findViewById( R.id.textView_desc );
        text.setText( "Check this for using on devices with blocked background service restriction" );

        // Autostart
        boolean autostart = false;
        if( sharedpreferences.contains( PREF_AUTOSTART ) )
        {
            autostart = sharedpreferences.getBoolean( PREF_AUTOSTART, false );
        }
        checkbox_autostart.setChecked( autostart );

        checkbox_autostart.setOnCheckedChangeListener( new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged( CompoundButton compoundButton, boolean b )
            {
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putBoolean( PREF_AUTOSTART, b );
                editor.apply();

                if( b )
                {
                    if( !Settings.canDrawOverlays( getApplicationContext() ) )
                    {
                        int ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE = 5469;
                        Intent intent = new Intent( Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse( "package:" + getPackageName() ) );
                        startActivityForResult( intent, ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE );
                    }
                }
            }
        } );

        // Bold
        boolean bold_font = false;

        if( sharedpreferences.contains( PREF_BOLDFONT ) )
        {
            bold_font = sharedpreferences.getBoolean( PREF_BOLDFONT, false );
        }

        checkbox_boldfont.setChecked( bold_font );

        checkbox_boldfont.setOnCheckedChangeListener( ( compoundButton, b ) -> {
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putBoolean( PREF_BOLDFONT, b );
            editor.apply();
        } );
    }
}
