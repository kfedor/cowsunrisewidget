package ru.cowsoft.cowsunrise;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class ClockUpdater
{
    public static void scheduleUpdate( Context context )
    {
        try
        {
            //Log.w( "AA", "Schedule update" );
            AlarmManager alarmManager = (AlarmManager) context.getSystemService( Context.ALARM_SERVICE );

            if( alarmManager == null )
            {
                return;
            }

            PendingIntent pendingIntent = getAlarmIntent( context, CowSunriseWidget.ACTION_UPDATE );

            long current = System.currentTimeMillis();

            // Надо запуститься ровно в 00
            long desired_time = current / 60000;

            //Log.w( "AA", "Current time: " + current );
            //Log.w( "AA", "Desired time: " + ( desired_time + 1 ) * 60000 );

            //alarmManager.setExact( AlarmManager.RTC_WAKEUP, ( desired_time + 1 ) * 60000, pendingIntent );
            alarmManager.setExactAndAllowWhileIdle( AlarmManager.RTC_WAKEUP, ( desired_time + 1 ) * 60000, pendingIntent );
        }
        catch( NullPointerException e )
        {
            e.printStackTrace();
        }
    }

    public static PendingIntent getAlarmIntent( Context context, String action )
    {
        Intent intent = new Intent( context, CowSunriseWidget.class );
        intent.setAction( action );
        return PendingIntent.getBroadcast( context, 0, intent, 0 );
    }

    public static void clearUpdate( Context context )
    {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService( Context.ALARM_SERVICE );

        if( alarmManager == null )
        {
            return;
        }

        alarmManager.cancel( getAlarmIntent( context, CowSunriseWidget.ACTION_UPDATE ) );
        alarmManager.cancel( getAlarmIntent( context, CowSunriseWidget.ACTION_UPDATE_MANUAL ) );
    }
}