package ru.cowsoft.cowsunrise;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.Log;

import java.util.Date;

import androidx.core.content.res.ResourcesCompat;

import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static ru.cowsoft.cowsunrise.CowSunriseWidget.rad_diff;
import static ru.cowsoft.cowsunrise.CowSunriseWidget.rad_ext;
import static ru.cowsoft.cowsunrise.CowSunriseWidget.rad_int;
import static ru.cowsoft.cowsunrise.CowSunriseWidget.size;
import static ru.cowsoft.cowsunrise.CowSunriseWidget.stroke;

class ClockDraw
{
    static void drawCowWidget( Context context, Canvas canvas, int sunrise_min, int daylength_min, boolean is_bold, int rising_status )
    {
        canvas.drawColor( Color.TRANSPARENT );

        Paint paint = new Paint();
        paint.setAntiAlias( true );

        // Темное время
        paint.setColor( context.getResources().getColor( R.color.peacock ) );
        paint.setStyle( Paint.Style.FILL );
        canvas.drawCircle( size / 2, size / 2, rad_ext, paint );

        // Светлое время
        paint.setColor( context.getResources().getColor( R.color.daylight ) );
        paint.setStyle( Paint.Style.FILL );
        canvas.drawArc(
                (float) size / 2 - rad_ext,
                (float) size / 2 - rad_ext,
                (float) size / 2 + rad_ext,
                (float) size / 2 + rad_ext,
                (float) ( ( sunrise_min ) / 4 ) - 90,   // Время рассвета в минутах
                (float) ( ( daylength_min ) / 4 ), // Время светлого дня в минутах в градусы
                true, paint );

        // Внешний круг
        paint.setStyle( Paint.Style.STROKE );
        paint.setColor( Color.WHITE );
        paint.setStrokeWidth( stroke );
        canvas.drawCircle( size / 2, size / 2, rad_ext, paint );

        // Внутренний круг
        canvas.drawCircle( size / 2, size / 2, rad_int, paint );

        // Разметка основная
        paint.setStyle( Paint.Style.STROKE );
        paint.setColor( Color.WHITE );
        paint.setStrokeWidth( stroke * 3 );
        DrawCurve( canvas, paint, size / 2, size / 2, rad_ext, rad_diff, 360, 15 );

        // Черная серединка
        //paint.setColor(Color.BLACK);
        //paint.setStrokeWidth(stroke);
        //DrawCurve(canvas, paint, size / 2, size / 2, rad_ext + stroke, rad_diff + stroke, 360, 15);

        // Разметка крест
        paint.setColor( Color.WHITE );
        paint.setStrokeWidth( stroke * 3 );
        DrawCurve( canvas, paint, size / 2, size / 2, rad_ext + 50, rad_diff + 50, 360, 90 );

        // Часы
        DrawClock( context, canvas, is_bold );

        // День растет или сокращается
        DrawArrow( canvas, paint, size / 2, size - size / 3, rising_status );

        // Равноденствие ( 11:55 -- 12:05 )
        if( daylength_min > 11 * 60 + 54 && daylength_min < 12 * 60 + 6 )
        {
            paint.setColor( Color.YELLOW );
            paint.setStyle( Paint.Style.STROKE );
            paint.setStrokeWidth( 10 );
            canvas.drawCircle( size / 2, size / 2, rad_int - 50, paint );
        }
    }

    private static void DrawClock( Context context, Canvas canvas, boolean is_bold )
    {
        //Log.w( "AA", "DrawClock()" );

        Paint paint = new Paint();
        Date date = java.util.Calendar.getInstance().getTime();

        // Заполним центр чтоб нарисовать текущее время
        paint.setStyle( Paint.Style.FILL );
        paint.setColor( Color.BLACK );
        canvas.drawCircle( size / 2, size / 2, rad_int - stroke, paint );

        // Мелкая разметка для времени
        paint.setStrokeWidth( 4 );
        paint.setColor( Color.WHITE );
        DrawCurve( canvas, paint, size / 2, size / 2, rad_int - stroke, rad_diff, 360, 3 );

        // Время - дуга красная
        paint.setColor( Color.RED );
        canvas.drawArc(
                size / 2 - rad_int + stroke,
                size / 2 - rad_int + stroke,
                size / 2 + rad_int - stroke,
                size / 2 + rad_int - stroke,
                270,   // Время рассвета в минутах
                ( ( date.getHours() * 60 + date.getMinutes() ) / 4 ), // Время светлого дня в минутах в градусы
                true, paint );

        // Заполним центр
        paint.setStyle( Paint.Style.FILL );
        paint.setColor( Color.BLACK );
        canvas.drawCircle( size / 2, size / 2, rad_int - 50, paint );

        // Напишем время
        paint.setColor( Color.WHITE );
        paint.setStyle( Paint.Style.FILL );
        Typeface fnt = ResourcesCompat.getFont( context, R.font.digital5 );

        if( is_bold )
        {
            Typeface bold = Typeface.create( fnt, Typeface.BOLD );
            paint.setTypeface( bold );
        }
        else
        {
            paint.setTypeface( fnt );
        }

        paint.setTextSize( 250 );

        String text = String.format( "%d:%02d", date.getHours(), date.getMinutes() );

        //Log.w( "AA", "DaTE: " + text );

        float text_width = paint.measureText( text );
        Rect bounds = new Rect();
        paint.getTextBounds( text, 0, text.length(), bounds );
        canvas.drawText( text, size / 2 - text_width / 2, size / 2 + bounds.height() / 2, paint );
    }

    // leng это внутреннее смешение от rad_ext
    private static void DrawCurve( Canvas canvas, Paint paint, int x, int y, int rad_ext, int leng, int grad_stop, int step )
    {
        double x1, y1;
        double x2, y2;

        for( int i = 0; i <= grad_stop; i += step )
        {
            double tcos = cos( i * PI / 180 );
            double tsin = sin( i * PI / 180 );
            x1 = rad_ext * tcos;
            y1 = rad_ext * tsin;
            x2 = ( rad_ext - leng ) * tcos;
            y2 = ( rad_ext - leng ) * tsin;

            canvas.drawLine( x + (int) x2, y + (int) y2, x + (int) x1, y + (int) y1, paint );
        }
    }

    /*
        rising_status
        1 - rising
        2 - falling
        3 - equal
     */
    private static void DrawArrow( Canvas canvas, Paint paint, int x, int y, int rising_status )
    {
        paint.setColor( Color.WHITE );

        switch( rising_status )
        {
            case 1:
                canvas.drawLine( x - 40, y + 30, x, y - 30, paint ); // /
                canvas.drawLine( x + 40, y + 30, x, y - 30, paint ); // \
                break;
            case 2:
                canvas.drawLine( x - 40, y - 30, x, y + 30, paint ); // /
                canvas.drawLine( x + 40, y - 30, x, y + 30, paint ); // \
                break;
            case 3:
                paint.setStyle( Paint.Style.FILL );
                paint.setColor( Color.YELLOW );
                canvas.drawCircle( x, y, 30, paint );
                break;
        }
    }
}
