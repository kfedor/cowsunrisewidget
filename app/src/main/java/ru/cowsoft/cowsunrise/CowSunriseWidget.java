package ru.cowsoft.cowsunrise;

import android.Manifest;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Build;
import android.os.StrictMode;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import androidx.core.app.ActivityCompat;

import static ru.cowsoft.cowsunrise.PermissionActivity.PREF_AUTOSTART;
import static ru.cowsoft.cowsunrise.PermissionActivity.PREF_BOLDFONT;

public class CowSunriseWidget extends AppWidgetProvider
{
    public static final String ACTION_UPDATE = "ru.cowsoft.cowsunrise.action.UPDATE";
    public static final String ACTION_UPDATE_MANUAL = "ru.cowsoft.cowsunrise.action.UPDATE_MANUAL";

    final public static int size = 1000; // 1000x1000
    final public static int rad_ext = size / 2 - 50;
    final public static int rad_int = size / 2 - 180;
    final public static int rad_diff = rad_ext - rad_int;
    final public static int stroke = 4;

    public static final String mypreference = "cowsunrise";
    private SharedPreferences sharedpreferences;
    public static final String PREF_LONG = "longitude";
    public static final String PREF_LAT = "latitude";
    public static final String PREF_SUNRISE_MIN = "sunrise_time"; // время в минутах с начала дня
    public static final String PREF_DAYLENGTH_MIN = "daylength";
    public static final String PREF_LASTCHECK = "last_check_time";
    public static final String PREF_DAY_RISING = "is_rising";

    private Context mContext;

    void updateAppWidget( final Context context, AppWidgetManager appWidgetManager, int appWidgetId )
    {
        Log.w( "CowSunrise", "updateAppWidget()" );

        try
        {
            mContext = context;
            sharedpreferences = context.getSharedPreferences( mypreference, Context.MODE_PRIVATE );

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy( policy );

            // Update data
            long last_update_time = 0;

            if( sharedpreferences.contains( PREF_LASTCHECK ) )
            {
                last_update_time = sharedpreferences.getLong( PREF_LASTCHECK, 0 );
            }

            Log.w( "CowSunrise", "Last update time: " + last_update_time );

            // Once a day
            if( last_update_time + 24 * 60 * 60 * 1000 < System.currentTimeMillis() )
            {
                checkLocationAndNetwork( context );
            }

            Bitmap bitmap = Bitmap.createBitmap( size, size, Bitmap.Config.ARGB_8888 );
            Canvas canvas = new Canvas( bitmap );

            int sunrise_min = 0;
            int daylength_min = 0;
            int rising_status = 0;
            boolean is_bold = false;

            if( sharedpreferences.contains( PREF_SUNRISE_MIN ) && sharedpreferences.contains( PREF_DAYLENGTH_MIN ) )
            {
                sunrise_min = sharedpreferences.getInt( PREF_SUNRISE_MIN, 0 );
                daylength_min = sharedpreferences.getInt( PREF_DAYLENGTH_MIN, 0 );
                rising_status = sharedpreferences.getInt( PREF_DAY_RISING, 0 );
            }

            if( sharedpreferences.contains( PREF_BOLDFONT ) )
            {
                is_bold = sharedpreferences.getBoolean( PREF_BOLDFONT, false );
            }

            Log.w( "CowSunrise", "Rising status: " + rising_status );
            ClockDraw.drawCowWidget( context, canvas, sunrise_min, daylength_min, is_bold, rising_status );

            RemoteViews views = new RemoteViews( context.getPackageName(), R.layout.cow_sunrise_widget );
            views.setImageViewBitmap( R.id.widget_image, bitmap );

            // Click update
            views.setOnClickPendingIntent( R.id.widget_image, ClockUpdater.getAlarmIntent( context, CowSunriseWidget.ACTION_UPDATE_MANUAL ) );

            // Instruct the widget manager to update the widget
            appWidgetManager.updateAppWidget( appWidgetId, views );
        }
        catch( Exception ex )
        {
            ex.printStackTrace();
        }
    }

    @Override
    public void onUpdate( Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds )
    {
        Log.w( "CowSunrise", "onUpdate()" );

        ClockUpdater.scheduleUpdate( context );

        // There may be multiple widgets active, so update all of them
        for( int appWidgetId : appWidgetIds )
        {
            updateAppWidget( context, appWidgetManager, appWidgetId );
        }
    }

    private void updateClock( Context context )
    {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance( context );

        ComponentName thisAppWidgetComponentName = new ComponentName( context.getPackageName(), getClass().getName() );
        int[] appWidgetIds = appWidgetManager.getAppWidgetIds( thisAppWidgetComponentName );

        onUpdate( context, appWidgetManager, appWidgetIds );
    }


    @Override
    public void onDeleted( Context context, int[] appWidgetIds )
    {
    }

    @Override
    public void onEnabled( Context context )
    {
        Log.w( "CowSunrise", "onEnabled()" );
        ClockUpdater.scheduleUpdate( context );
    }

    @Override
    public void onDisabled( Context context )
    {
        ClockUpdater.clearUpdate( context );
    }

    @Override
    public void onReceive( Context context, Intent intent )
    {
        sharedpreferences = context.getSharedPreferences( mypreference, Context.MODE_PRIVATE );

        if( Intent.ACTION_BOOT_COMPLETED.equals( intent.getAction() ) )
        {
            if( sharedpreferences.contains( PREF_AUTOSTART ) && sharedpreferences.getBoolean( PREF_AUTOSTART, false ) )
            {
                try
                {
                    Intent app_intent = new Intent( context, PermissionActivity.class );
                    app_intent.putExtra( "startService", true );
                    //app_intent.setFlags( Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS );
                    app_intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
                    context.startActivity( app_intent );
                }
                catch( Exception ex )
                {
                    Toast.makeText( context, "Exception" + ex.getMessage(), Toast.LENGTH_LONG ).show();
                }
            }
        }
        else if( ACTION_UPDATE.equals( intent.getAction() ) )
        {
            updateClock( context );
        }
        else if( ACTION_UPDATE_MANUAL.equals( intent.getAction() ) )
        {
            checkLocationAndNetwork( context );
            updateClock( context );
        }
        else
        {
            super.onReceive( context, intent );
        }
    }

    void checkLocationAndNetwork( final Context context )
    {
        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.M )
        {
            if( ActivityCompat.checkSelfPermission( context, Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission( context, Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED )
            {
                Intent intent = new Intent( context, PermissionActivity.class );
                intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
                context.startActivity( intent );
            }
        }

        FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient( context );

        fusedLocationClient.getLastLocation().addOnSuccessListener( location -> {
            if( location != null )
            {
                double lat = location.getLatitude();
                double lon = location.getLongitude();

                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putFloat( PREF_LAT, (float) lat );
                editor.putFloat( PREF_LONG, (float) lon );

                getNetworkData( lat, lon );

                //
                Date date = new Date();

                int rising_status = 0;

                // Солцестояние
                Date SunStayWinter = new Date( date.getYear(), 12, 21 );
                Date SunStaySummer = new Date( date.getYear(), 6, 20 );

                if( date.equals( SunStaySummer ) || date.equals( SunStayWinter ) )
                {
                    rising_status = 3; // switch day
                }
                else if( date.after( SunStaySummer ) && date.before( SunStayWinter ) )
                {
                    if( lat > 0 )
                    {
                        rising_status = 2; // falling north hemisphere
                    }
                    else
                    {
                        rising_status = 1; // rising south hemisphere
                    }
                }
                else
                {
                    if( lat > 0 )
                    {
                        rising_status = 1; // rising north hemisphere
                    }
                    else
                    {
                        rising_status = 2; // falling south hemisphere
                    }
                }

                editor.putInt( PREF_DAY_RISING, rising_status );
                editor.apply();
            }
        } );
    }

    void getNetworkData( double lat, double lon )
    {
        HttpURLConnection urlConnection = null;

        Log.w( "CowSunriseWidget", "getNetworkData()" );

        try
        {
            String requestUrl = "http://api.sunrise-sunset.org/json?lat=" + lat + "&lng=" + lon;

            URL url = new URL( requestUrl );
            urlConnection = (HttpURLConnection) url.openConnection();

            StringBuilder response = new StringBuilder();
            BufferedReader reader = new BufferedReader( new InputStreamReader( urlConnection.getInputStream() ) );
            String line;

            while( ( line = reader.readLine() ) != null )
            {
                response.append( line );
            }
            reader.close();

            parseJsonReply( response.toString() );
        }
        catch( Exception ex )
        {
            ex.printStackTrace();
        }
        finally
        {
            if( urlConnection != null )
            {
                urlConnection.disconnect();
            }
        }
    }

    private boolean parseJsonReply( String resp )
    {
        try
        {
            JSONObject mainObject = new JSONObject( resp );

            if( !mainObject.isNull( "status" ) )
            {
                String status = mainObject.getString( "status" );

                if( !status.equals( "OK" ) )
                {
                    return false;
                }

                JSONObject results = mainObject.getJSONObject( "results" );
                String sunrise = results.getString( "sunrise" );
                String day_length = results.getString( "day_length" );

                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putInt( PREF_SUNRISE_MIN, AMPMTimeToMinutes( sunrise, "hh:mm:ss a" ) );

                int newDayLengthMin = DayLengthToMinutes( day_length );
                editor.putInt( PREF_DAYLENGTH_MIN, newDayLengthMin );
                editor.putLong( PREF_LASTCHECK, System.currentTimeMillis() );

                editor.apply();
            }
        }
        catch( Exception ex )
        {
            ex.printStackTrace();
        }

        return true;
    }

    private int DayLengthToMinutes( String daylength )
    {
        String[] parts = daylength.split( ":" );
        return Integer.parseInt( parts[0] ) * 60 + Integer.parseInt( parts[1] );
    }

    private int AMPMTimeToMinutes( String AMPMTime, String format )
    {
        try
        {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat parseFormat = new SimpleDateFormat( format, Locale.US );

            parseFormat.setTimeZone( TimeZone.getTimeZone( "UTC" ) );

            Date date = parseFormat.parse( AMPMTime );
            cal.setTime( date );

            return cal.get( Calendar.HOUR ) * 60 + cal.get( Calendar.MINUTE );
        }
        catch( Exception e )
        {
            e.printStackTrace();
            Toast.makeText( mContext, e.getMessage(), Toast.LENGTH_LONG ).show();
        }
        return 0;
    }
}
